class SearchSuggestionsController < ApplicationController
  def index
    p "auto suggest called with params #{params}"
     render json: SearchSuggestion.terms_for(params[:term])
  end
end
