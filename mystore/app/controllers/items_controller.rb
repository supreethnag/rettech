class ItemsController < ApplicationController
  def index
    p "index"
  	@Items=Item.all  	
  end

  def auto
    render json: Item.term_for(params[:name])
  end

  def search
    p "search"
  end

  def show
    p "show"
    @Item=Item.find(params[:id].to_i)
  end

  def create
    p "create"
  	@Item=Item.new(:name => params["items"]["name"], :price => params["items"]["price"])
  	if @Item.save
    	redirect_to @Item
 	else
    	render 'new'
  	end  	
  end

  def edit
    p "edit"
  	@Item=Item.find(params[:id].to_i)  	
  end

  def update
    p "update"
  	@Item=Item.find(params[:id].to_i)
  	if(@Item.update_attributes(:name => params["item"]["name"], :price => params["item"]["price"].to_i))
  		redirect_to @Item
  	else
  		render 'edit'
  	end
  end

  def bill
    p "bill"
  end

  def calculate_bill
    p "calculate bill"
  end

  def all
    render json: Item.select(:name,:price,:id)
  end

  def price_by_name
    item=Item.find_by_name(params[:name])
    render json: item.price
  end


  private
  def item_params
    params.require(:item).permit(:name, :price)
  end
end