class SearchSuggestion < ActiveRecord::Base

  def user_suggestion_params
    params.require(:SearchSuggestion).permit(:term, :popularity)
  end

  def self.terms_for(prefix)
    suggestions = where("term like ?","#{prefix}%")
    suggestions.order("popularity desc").limit(10).pluck(:term)
  end

  def self.index_products
    puts "items  are #{Item.all}"
    Item.find_each do |item|
      puts "item name is #{item.name}"
      index_term(item.name)
      item.name.split.each {|t| index_term(t)}
    end
  end

  def self.index_term(term)
    where(term: term.downcase).first_or_initialize.tap do |suggestion|
      suggestion.increment! :popularity
    end

  end
end

